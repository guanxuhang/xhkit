#ifdef __OBJC__
#import <UIKit/UIKit.h>
#endif

#import "NSData+XHKit.h"
#import "NSObject+XHKit.h"
#import "NSString+XHKit.h"
#import "UIButton+XHKit.h"
#import "UIColor+XHKit.h"
#import "UIFont+XHKit.h"
#import "UIImage+XHKit.h"
#import "UIImageView+XHKit.h"
#import "UILabel+XHKit.h"
#import "UIView+XHKit.h"
#import "UIViewController+XHKit.h"

FOUNDATION_EXPORT double XHKitVersionNumber;
FOUNDATION_EXPORT const unsigned char XHKitVersionString[];

