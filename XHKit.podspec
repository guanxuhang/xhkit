#
# Be sure to run `pod lib lint ${POD_NAME}.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'XHKit'
  s.version          = '0.2.1'
  s.summary          = '快捷开发'


  s.homepage         = 'https://guanxuhang@bitbucket.org/guanxuhang/xhkit.git'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { "关旭航" => "18611817195@163.com" }
  s.source           = { :git => "https://guanxuhang@bitbucket.org/guanxuhang/xhkit.git", :tag => s.version.to_s }
  s.ios.deployment_target = '7.0'
  s.requires_arc = true

  s.source_files = 'XHKit/classes/*.{h,m}'

  s.subspec 'Category' do |ss1|
  ss1.source_files = 'XHKit/classes/Category/*.{h,m}'
  end

  s.subspec 'component' do |ss2|
  ss2.source_files = 'XHKit/classes/component/*/**.{h,m}'
  end

  # s.resource_bundles = {
  #   '${POD_NAME}' => ['${POD_NAME}/Assets/*.png']
  # }

  # s.public_header_files = 'Pod/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'
  # s.dependency 'AFNetworking', '~> 2.3'
end
