//
//  AppDelegate.h
//  XHKit
//
//  Created by guanxuhang1234 on 2017/5/19.
//  Copyright © 2017年 liqing. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

