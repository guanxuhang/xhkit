//
//  UILabel+XHKit.m
//  PlayTogether
//
//  Created by 何伟东 on 2016/11/9.
//  Copyright © 2016年 何伟东. All rights reserved.
//

#import "UILabel+XHKit.h"
#import "UIView+XHKit.h"


@implementation UILabel (XHKit)
+ (UILabel *)labelWithTitle:(NSString *)title textAlignment:(NSTextAlignment)textAlignment font:(UIFont *)font color:(UIColor*)color{
    UILabel * label = [[UILabel alloc] init];
    label.text = title;
    label.textColor = color;
    label.textAlignment = textAlignment;
    label.font = font;
    return label;
}
-(void)labelWithTitle:(NSString *)title textAlignment:(NSTextAlignment)textAlignment font:(UIFont *)font color:(UIColor*)color{
    self.text = title;
    self.textColor = color;
    self.textAlignment = textAlignment;
    self.font = font;
}
- (float)contentHeightWithLabelWidth:(float)width{
    
    CGSize size = CGSizeMake(width,MAXFLOAT);//跟label的宽设置一样
    
    size = [self sizeThatFits:size];
    
    
    return size.height+1;
}
- (float)contentHeight{
    CGSize size = CGSizeMake(self.width,MAXFLOAT);//跟label的宽设置一样
    
    size = [self sizeThatFits:size];
    
    return size.height+1;
}
- (float)contentWidth{
    CGSize size = CGSizeMake(MAXFLOAT,self.height);//跟label的宽设置一样
    
    size = [self sizeThatFits:size];
    
    
    return size.width+1;
}


@end
