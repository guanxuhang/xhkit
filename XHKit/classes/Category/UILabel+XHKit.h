//
//  UILabel+XHKit.h
//  PlayTogether
//
//  Created by 何伟东 on 2016/11/9.
//  Copyright © 2016年 何伟东. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UILabel (XHKit)

/**
 *  创建Label
 *
 */
+ (UILabel *)labelWithTitle:(NSString *)title textAlignment:(NSTextAlignment)textAlignment font:(UIFont *)font color:(UIColor*)color;
-(void)labelWithTitle:(NSString *)title textAlignment:(NSTextAlignment)textAlignment font:(UIFont *)font color:(UIColor*)color;
- (float)contentHeight;
- (float)contentHeightWithLabelWidth:(float)width;

- (float)contentWidth;
@end
