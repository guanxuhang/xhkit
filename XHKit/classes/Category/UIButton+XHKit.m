//
//  UIButton+XHKit.m
//  XHKit
//
//  Created by 何伟东 on 2016/11/1.
//  Copyright © 2016年 何伟东. All rights reserved.
//

#import "UIButton+XHKit.h"
#import "UIImage+XHKit.h"
#import <objc/runtime.h>


static const void *XHHandlersKey = &XHHandlersKey;

@interface HandlerInvoke : NSObject <NSCopying>{}

- (id)initWithHandler:(void (^)(id sender))handler forControlEvents:(UIControlEvents)controlEvents;
@property (nonatomic) UIControlEvents controlEvents;
@property (nonatomic, copy) void (^handler)(id sender);

@end

@implementation HandlerInvoke

- (id)initWithHandler:(void (^)(id sender))handler forControlEvents:(UIControlEvents)controlEvents{
    self = [super init];
    if (self) {
        self.handler = handler;
        self.controlEvents = controlEvents;
    }
    return self;
}

- (id)copyWithZone:(NSZone *)zone{
    return [[HandlerInvoke alloc] initWithHandler:self.handler forControlEvents:self.controlEvents];
}

- (void)invoke:(id)sender{
    self.handler(sender);
}

-(void)dealloc{
    
}

@end


@implementation UIButton (XHKit)


+ (UIButton *)xh_buttonWithNormalTitle:(NSString *)title selectedTitle:(NSString *)selectedtitle font:(UIFont *)font NormaltitleColor:(UIColor *)color selectedTitleColor:(UIColor *)selectedColor normalImage:(UIImage *)image selectedImage:(UIImage *)selectedImage withBlock:(void(^)(id sender))block{
    UIButton * btn = [[UIButton alloc]init];
    [btn setTitle:title forState:UIControlStateNormal];
    btn.titleLabel.font = font;
    [btn setTitleColor:color forState:UIControlStateNormal];
    if (block) {
        [btn handlerControlEvent:UIControlEventTouchUpInside handler:block];
    }
    if (selectedColor) {
        [btn setTitleColor:selectedColor forState:UIControlStateSelected];
    }
    if (selectedtitle) {
        [btn setTitle:title forState:UIControlStateSelected];
    }
    if (image) {
        [btn setImage:image forState:UIControlStateNormal];
    }
    if (selectedImage) {
        [btn setImage:selectedImage forState:UIControlStateSelected];
    }
    return btn;
}

+ (UIButton *)xh_buttonWithTitle:(NSString *)title font:(UIFont *)font titleColor:(UIColor *)color withBlock:(void(^)(id sender))block{
    return  [self xh_buttonWithNormalTitle:title selectedTitle:nil font:font NormaltitleColor:color selectedTitleColor:nil normalImage:nil selectedImage:nil withBlock:block];
}

+ (UIButton *)xh_buttonWithTitle:(NSString *)title font:(UIFont *)font titleColor:(UIColor *)color normalImage:(UIImage *)image withBlock:(void(^)(id sender))block{
    return  [self xh_buttonWithNormalTitle:title selectedTitle:nil font:font NormaltitleColor:color selectedTitleColor:nil normalImage:image selectedImage:nil withBlock:block];
}
- (void)xh_buttonWithTitle:(NSString *)title font:(UIFont *)font titleColor:(UIColor *)color withBlock:(void(^)(id sender))block{
    [self xh_buttonWithNormalTitle:title selectedTitle:nil font:font NormaltitleColor:color selectedTitleColor:nil normalImage:nil selectedImage:nil withBlock:block];
}
- (void)xh_buttonWithTitle:(NSString *)title font:(UIFont *)font titleColor:(UIColor *)color normalImage:(UIImage *)image withBlock:(void(^)(id sender))block{
    [self xh_buttonWithNormalTitle:title selectedTitle:nil font:font NormaltitleColor:color selectedTitleColor:nil normalImage:image selectedImage:nil withBlock:block];
}
- (void)xh_buttonWithNormalTitle:(NSString *)title selectedTitle:(NSString *)selectedtitle font:(UIFont *)font NormaltitleColor:(UIColor *)color selectedTitleColor:(UIColor *)selectedColor normalImage:(UIImage *)image selectedImage:(UIImage *)selectedImage withBlock:(void(^)(id sender))block{
    [self setTitle:title forState:UIControlStateNormal];
    self.titleLabel.font = font;
    [self setTitleColor:color forState:UIControlStateNormal];
    if (block) {
        [self handlerControlEvent:UIControlEventTouchUpInside handler:block];
    }
    if (selectedColor) {
        [self setTitleColor:selectedColor forState:UIControlStateSelected];
    }
    if (selectedtitle) {
        [self setTitle:title forState:UIControlStateSelected];
    }
    if (image) {
        [self setImage:image forState:UIControlStateNormal];
    }
    if (selectedImage) {
        [self setImage:selectedImage forState:UIControlStateSelected];
    }
}

/**
 创建便捷的通用点击事件
 
 @param controlEvent button事件
 @param handler 回调block
 */
-(void)handlerControlEvent:(UIControlEvents)controlEvent handler:(void (^)(id sender))handler{
    NSMutableArray *events = objc_getAssociatedObject(self, XHHandlersKey);
    if (!events) {
        events = [NSMutableArray array];
        objc_setAssociatedObject(self, XHHandlersKey, events, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    }
    HandlerInvoke *target = [[HandlerInvoke alloc] initWithHandler:handler forControlEvents:controlEvent];
    [events addObject:target];
    [self addTarget:target action:@selector(invoke:) forControlEvents:controlEvent];
}

/**
 创建最常用的TouchUpInside点击
 
 @param handler 回调block
 */
-(void)handlerTouchUpInsideEvent:(void (^)(id sender))handler{
    [self handlerControlEvent:UIControlEventTouchUpInside handler:handler];
}

/**
 移除target
 
 @param event <#event description#>
 */
-(void)removeTargetWithEvent:(UIControlEvents)event{
    NSMutableArray *events = objc_getAssociatedObject(self, XHHandlersKey);
    NSMutableArray *copyArray = [events mutableCopy];
    [copyArray enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        HandlerInvoke *target = events[idx];
        if (target.controlEvents == event) {
            [self removeTarget:target action:NULL forControlEvents:event];
            [events removeObject:target];
        }
    }];
}
/**
 *  设置不同状态背景颜色
 *
 */
- (void)setBackgroundColor:(UIColor *)backgroundColor forState:(UIControlState)state {
    [self setBackgroundImage:[UIImage imageWithColor:backgroundColor] forState:state];
    
}

@end



