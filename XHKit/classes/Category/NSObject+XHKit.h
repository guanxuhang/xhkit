//
//  NSObject+XHKit.h
//  XHKit
//
//  Created by 何伟东 on 2016/12/9.
//  Copyright © 2016年 何伟东. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSObject (XHKit)

/**
 runtime 扩展属性
 */
@property (nonatomic,strong) NSMutableDictionary *extentObject;

@end
