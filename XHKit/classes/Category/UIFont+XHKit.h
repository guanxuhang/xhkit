//
//  UIFont+XHKit.h
//  XHKit
//
//  Created by 何伟东 on 2016/11/1.
//  Copyright © 2016年 何伟东. All rights reserved.
//

#import <UIKit/UIKit.h>
//便捷创建字体字体
#define SystemFont(size) [UIFont systemFontOfSize:size]
//便捷创建系统粗字体
#define BoldSystemFont(size) [UIFont boldSystemFontOfSize:size]

@interface UIFont (XHKit)

@end
