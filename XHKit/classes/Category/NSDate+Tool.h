//
//  NSDate+Tool.h
//  GEDU_Demo
//
//  Created by Eric on 14/10/29.
//  Copyright (c) 2014年 Eric. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDate (Tool)

- (BOOL)isSameDay:(NSDate*)anotherDate;

- (NSInteger)secondsAgo;
- (NSInteger)minutesAgo;
- (NSInteger)hoursAgo;
- (NSInteger)monthsAgo;
- (NSInteger)yearsAgo;

- (NSString *)stringTimesAgo;
- (NSString *)string_yyyy_MM_dd_EEE;
- (NSString *)stringTimeDisplay;

- (NSString *)defaultTimeStr;
- (NSString *)dayString;
- (NSString *)string_yyyy_MM_dd;
- (NSString *)string_a_HH_mm;

- (NSString *)defaultchineseTimeStr;

- (BOOL)isNight;
//当前是第几小时
- (NSInteger)currentHours;

+ (NSString *)convertStr_yyyy_MM_ddToDisplay:(NSString *)str_yyyy_MM_dd;
/**
 *  北京时间的时间戳转时间
 *
 *  @param time   时间戳字符串  10位
 *  @param format 格式  可以传nil  默认为@"yyyy-MM-dd HH:mm"
 *
 *  @return 时间格式
 */
+ (NSString *)convertBeijingTimeWithBeijingTimeStamp:(NSString *)time WithFormat:(NSString *)format;
/**
 *  得到北京时间的时间戳转
 *
 *  @param date  可以传nil  当前时间
 *
 *  @return 北京时间的时间戳转
 */
+ (NSString *)convertBeijingTimeWithDate:(NSDate*)date;
+ (NSDate *)getUTC_DateWithBeiJingLongTime:(NSString *)beijingTimeStamp;
+ (NSString *)getUTCTimeStampWithBeiJingLongTime:(NSString *)beijingTimeStamp;
@end
