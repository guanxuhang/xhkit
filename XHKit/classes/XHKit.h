//
//  XHKit.h
//  XHKit
//
//  Created by guanxuhang1234 on 2017/5/19.
//  Copyright © 2017年 liqing. All rights reserved.
//
//3.5寸屏
#define is3_5inch ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(640, 960), [[UIScreen mainScreen] currentMode].size) : NO)
//3.5寸retina屏
#define is4inch_retina ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(640, 1136), [[UIScreen mainScreen] currentMode].size) : NO)
//4.7寸retina屏
#define is4_7inch_retina ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(750, 1334), [[UIScreen mainScreen] currentMode].size) : NO)
//5.寸retina屏
#define is5_5inch_retina ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(1242, 2208), [[UIScreen mainScreen] currentMode].size) : NO)

//app框架高度
#define APP_FRAME_HEIGHT [[UIScreen mainScreen] applicationFrame].size.height
//app框架宽度
#define APP_FRAME_WIDTH [[UIScreen mainScreen] applicationFrame].size.width
//屏幕的尺寸
#define SCREEN_SIZE ([[UIScreen mainScreen] bounds]).size
//屏幕高度
#define SCREEN_HEIGHT ([[UIScreen mainScreen] bounds]).size.height
//屏幕宽度
#define SCREEN_WIDTH ([[UIScreen mainScreen] bounds]).size.width

//沙盒目录
#define DOCUMENT_PATH [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0]
//应用keyWindow
#define KEY_WINDOW [UIApplication sharedApplication].keyWindow


#define RGBA(R/*红*/, G/*绿*/, B/*蓝*/, A/*透明*/)\
[UIColor colorWithRed:R/255.f green:G/255.f blue:B/255.f alpha:A]


#define HEXCOLOR(hex)\
[UIColor colorWithHexString:hex]

// 调试
#ifdef DEBUG
#define DLog(fmt, ...) NSLog((@"%s [Line %d] " fmt), __PRETTY_FUNCTION__, __LINE__, ##__VA_ARGS__);
#else
#define DLog(...)
#endif


#ifndef XHKit_h
#define XHKit_h

//类别
#import "NSData+XHKit.h"
#import "NSString+XHKit.h"
#import "NSObject+XHKit.h"
#import "NSString+XHKit.h"
#import "UIButton+XHKit.h"
#import "UIImage+XHKit.h"
#import "UIColor+XHKit.h"
#import "UIFont+XHKit.h"
#import "UIImageView+XHKit.h"
#import "UILabel+XHKit.h"
#import "UIView+XHKit.h"
#import "UIViewController+XHKit.h"

//组件
#import "XHViewController.h"

#import "XHCustomerCollectionView.h"
#import "XHHorizonalController.h"
#import "XHMoreVcsParentVC.h"
#import "YPTabBar.h"
#import "YPTabItem.h"

#import "ZYBaseTableViewHeaderFooterView.h"


#endif /* XHKit_h */
