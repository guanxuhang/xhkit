//
//  XHViewController.h
//  NREnglish
//
//  Created by guanxuhang1234 on 2017/5/22.
//  Copyright © 2017年 liqing. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface XHViewController : UIViewController
/**
 页面路径
 */
@property(nonatomic,copy) NSString * path;
/**
 页面名称
 */
@property(nonatomic,copy) NSString * pageName;

@end
