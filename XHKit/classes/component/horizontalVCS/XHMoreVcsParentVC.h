//
//  ZYMoreVcsParentVC.h
//  TImerTest
//
//  Created by guanxuhang1234 on 16/6/15.
//  Copyright © 2016年 guanxuhang1234. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "XHHorizonalController.h"
#import "YPTabBar.h"
#import "YPTabItem.h"
#import "XHViewController.h"
@interface XHMoreVcsParentVC : XHViewController
@property (nonatomic, strong) XHHorizonalController *viewPager;//内容vc;
@property (nonatomic, strong) YPTabBar * columbarBar;//选项栏;
/**
 *  选项栏  高度   默认是45
 */
@property (nonatomic, assign) float columbarBarHeight;
/**
 *  是否可以滑动
 */
@property (nonatomic, assign) BOOL  isSwipe;
@property (nonatomic, assign) float  barWidth;


/**
 item是否是按照长度自然排序  默认NO  bar的宽度除以个数为每个item的宽度
 */
@property (nonatomic, assign) BOOL  isFitTextWidth;

/*
 #define NORMAL_COlOR [UIColor colorWithWhite:1 alpha:0.8]
 #define SELECT_COlOR [UIColor whiteColor]
 #define NORMAL_FONT [UIFont systemFontOfSize:13]
 #define SELECTL_FONT [UIFont systemFontOfSize:15]
 */

/**
  常态字体颜色  上面有默认值  非必须赋值
 */
@property (nonatomic, strong)UIColor * normalTextColor;
/**
 选中字体颜色 上面有默认值  非必须赋值
 */
@property (nonatomic, strong)UIColor * selectedTextColor;
@property (nonatomic, strong)UIFont * normalTextFont;
@property (nonatomic, strong)UIFont * selectedTextFont;


/**
 *  初始话
 *  @return ····
 */
/**
 *  选中的index
 */
@property (nonatomic, assign) NSInteger currentSelectedIndex;
- (id)initWithViewControllers:(NSArray *)viewControllers AndTitles:(NSArray *)titles WithBarIsOnNav:(BOOL)OnNav;
@property(nonatomic,copy)NSArray * controllers;
/**
 *  设置bar的属性
 */
- (void)set_ColumbarBar;
@end
