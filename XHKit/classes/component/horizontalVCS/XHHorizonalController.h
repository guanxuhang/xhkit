//
//  YZHorizonalController.h
//  TImerTest
//
//  Created by guanxuhang1234 on 16/6/15.
//  Copyright © 2016年 guanxuhang1234. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "XHCustomerCollectionView.h"
#import "XHViewController.h"
@interface XHHorizonalController :XHViewController

@property (nonatomic,copy) NSArray *controllers;
@property(nonatomic,strong)XHCustomerCollectionView * collectionView;
/**
 *  变为第几个item
 */
@property (nonatomic, copy) void (^changeIndex)(NSUInteger index);
/**
 *  scrollView移动
 */
@property (nonatomic, copy) void (^scrollView)(CGFloat offsetRatio, NSUInteger focusIndex, NSUInteger animationIndex);
/**
 *  第几个vc出现
 */
@property (nonatomic, copy) void (^viewDidAppear)(NSInteger index);
/**
 *  初始化
 *
 *  @param controller数组
 *
 *  @return ·····
 */
- (instancetype)initWithViewControllers:(NSArray *)controllers;
/**
 *  滑动第几个
 *
 *  @param index 第几个
 */
- (void)scrollToViewAtIndex:(NSUInteger)index;
/**
 *  刷新
 *
 *  @param controllers controller数组
 */
- (void)reloadViewsWithControllers:(NSArray *)controllers;
// 是否可以滑动
@property(nonatomic,assign)BOOL scrollEnabled;
@end
