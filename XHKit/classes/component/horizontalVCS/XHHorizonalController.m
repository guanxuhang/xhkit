//
//  YZHorizonalController.m
//  TImerTest
//
//  Created by guanxuhang1234 on 16/6/15.
//  Copyright © 2016年 guanxuhang1234. All rights reserved.
//

#import "XHHorizonalController.h"

static NSString *kHorizonalCellID = @"HorizonalCell";
@interface XHHorizonalController()<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>
{
    
    UICollectionViewFlowLayout *_flowLayout;
    UIViewController * currentController;
}
@end
@implementation XHHorizonalController
- (instancetype)initWithViewControllers:(NSArray *)controllers
{
    self = [super init];
    if (self) {
        _scrollEnabled = YES;
        _controllers = controllers;
        for (UIViewController *controller in controllers) {
            [self addChildViewController:controller];
        }
        if (controllers&&controllers.count>0) {
            currentController= controllers[0];
        }
    }
    return self;
}
- (void)reloadViewsWithControllers:(NSArray *)controllers{
    _controllers = controllers;
    if (controllers&&controllers.count>0) {
        currentController= controllers[0];
    }
    for (UIViewController *controller in controllers) {
        [controller removeFromParentViewController];
    }
    for (UIViewController *controller in controllers) {
        [self addChildViewController:controller];
    }
    [_collectionView reloadData];
}
- (void)setScrollEnabled:(BOOL)scrollEnabled{
    _scrollEnabled = scrollEnabled;
    if (_collectionView) {
        _collectionView.scrollEnabled = _scrollEnabled;
    }
}
- (void)initCollectionView{
    if (_collectionView) {
        [_collectionView removeFromSuperview];
        _collectionView= nil;
    }
    _flowLayout=[[UICollectionViewFlowLayout alloc] init];
    [_flowLayout setScrollDirection:UICollectionViewScrollDirectionHorizontal];
    _flowLayout.itemSize = CGSizeMake(self.view.frame.size.width,self.view.frame.size.height);
    _flowLayout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
    _flowLayout.minimumLineSpacing = 0;
    _collectionView = [[XHCustomerCollectionView alloc] initWithFrame:self.view.bounds collectionViewLayout:_flowLayout];
    _collectionView.backgroundColor = [UIColor clearColor];
    _collectionView.dataSource=self;
    _collectionView.delegate=self;
    _collectionView.showsHorizontalScrollIndicator = NO;
    _collectionView.showsVerticalScrollIndicator = NO;
    _collectionView.scrollEnabled = self.scrollEnabled;
    [_collectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:kHorizonalCellID];
    _collectionView.pagingEnabled = YES;
    _collectionView.bounces = NO;
   // _collectionView.scrollsToTop = NO;
    [self.view addSubview:_collectionView];
}
- (void)viewWillLayoutSubviews{
    
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    self.automaticallyAdjustsScrollViewInsets=NO;
    self.view.backgroundColor = [UIColor clearColor];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self initCollectionView];
    });
    
}
#pragma mark - collectionView的数据源方法
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return _controllers.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:kHorizonalCellID forIndexPath:indexPath];
    UIViewController *controller = _controllers[indexPath.row];
    controller.view.frame = cell.contentView.bounds;
    for (UIView * view in cell.contentView.subviews){
        [view removeFromSuperview];
    }
    [cell.contentView addSubview:controller.view];
    return cell;
}
- ( CGSize )collectionView:( UICollectionView *)collectionView layout:( UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:( NSIndexPath *)indexPath
{
    
    return CGSizeMake(self.view.frame.size.width,self.view.frame.size.height);
    
}

-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsMake(0, 0, 0, 0);
}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section{
    return 0;
}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section{
    return 0;
}

#pragma mark - <UIScrollViewDelegate>
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    [self scrollStop:YES];
    
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    [self scrollStop:NO];
}

- (void)scrollToViewAtIndex:(NSUInteger)index
{
    [_collectionView scrollToItemAtIndexPath:[NSIndexPath indexPathForRow:index inSection:0] atScrollPosition:UICollectionViewScrollPositionLeft  animated:NO];
    [self vcDisappearAndAppearWithIndex:index];
    if (_viewDidAppear) {
        _viewDidAppear(index);
    }
}

- (void)scrollStop:(BOOL)didScrollStop
{
    CGFloat horizonalOffset = _collectionView.contentOffset.x;
    CGFloat screenWidth = _collectionView.frame.size.width;
    CGFloat offsetRatio = (NSUInteger)horizonalOffset % (NSUInteger)screenWidth / screenWidth;
    NSUInteger focusIndex = (horizonalOffset + screenWidth / 2) / screenWidth;
    
    if (horizonalOffset != focusIndex * screenWidth) {
        NSUInteger animationIndex = horizonalOffset > focusIndex * screenWidth ? focusIndex + 1: focusIndex - 1;
        if (focusIndex > animationIndex) {offsetRatio = 1 - offsetRatio;}
        _scrollView(offsetRatio, focusIndex, animationIndex);
    }
    if (didScrollStop) {
        /*
         [_controllers enumerateObjectsUsingBlock:^(UIViewController *vc, NSUInteger idx, BOOL *stop) {
         if ([vc isKindOfClass:[UITableViewController class]]) {
         ((UITableViewController *)vc).tableView.scrollsToTop = (idx == focusIndex);
         }
         }];
         */
        [self vcDisappearAndAppearWithIndex:focusIndex];
        _changeIndex(focusIndex);
    }
}
- (void)vcDisappearAndAppearWithIndex:(NSInteger)index{
    UIViewController * currentVC= _controllers[index];
    if (currentVC!=currentController) {
        [currentController viewWillDisappear:NO];
        [currentController viewDidDisappear:NO];
        currentController= currentVC;
        [currentController viewWillAppear:NO];
        [currentController viewDidAppear:NO];
    }
    
}
@end
