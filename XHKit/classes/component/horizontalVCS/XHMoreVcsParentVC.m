//
//  ZYMoreVcsParentVC.m
//  TImerTest
//
//  Created by guanxuhang1234 on 16/6/15.
//  Copyright © 2016年 guanxuhang1234. All rights reserved.
//

#import "XHMoreVcsParentVC.h"


static const float barHeight = 45.0;//默认选项栏高度
#define NORMAL_COlOR [UIColor colorWithWhite:1 alpha:0.8]
#define SELECT_COlOR [UIColor whiteColor]
#define NORMAL_FONT [UIFont systemFontOfSize:13]
#define SELECTL_FONT [UIFont systemFontOfSize:15]
#define NORMAL_FONT_Little [UIFont systemFontOfSize:11]
#define SELECTL_FONT_Little [UIFont systemFontOfSize:12]
@interface XHMoreVcsParentVC()
{
    NSArray * _titles;
    BOOL _isOnNav;
}
@end
@implementation XHMoreVcsParentVC
- (id)initWithViewControllers:(NSArray *)viewControllers AndTitles:(NSArray *)titles WithBarIsOnNav:(BOOL)OnNav{
    self = [super init];
    if (self) {
        _isOnNav = OnNav;
        _titles = titles;
        _controllers =viewControllers;
        _isSwipe = YES;
        
    }
    return self;
}
- (void)initUI{
    if (_titles==nil) {
        return;
    }
    if (self.columbarBarHeight==0) {
        self.columbarBarHeight = barHeight;
    }
    __weak typeof(self)tempSelf = self;
    self.columbarBar = [[YPTabBar alloc] init];
    self.columbarBar.backgroundColor = [UIColor clearColor];
    if (_isOnNav) {
        self.columbarBar.frame = CGRectMake(0, 0,_barWidth==0?self.view.frame.size.width-16:_barWidth,44);
        self.navigationItem.titleView = self.columbarBar;
    }else{
        self.columbarBar.frame = CGRectMake(0, 0, self.view.frame.size.width-16,self.columbarBarHeight);
        [self.view addSubview:self.columbarBar];
    }
    [self set_ColumbarBar];
    [self.columbarBar setTitles:_titles];
    self.columbarBar.selectedItemIndex = 0;
    self.columbarBar.willSelectItemBlock = ^(NSInteger index){
        
    };
    self.columbarBar.didSelectItemBlock = ^(NSInteger index){
        [tempSelf.viewPager scrollToViewAtIndex:index];
        [tempSelf setCollectionViewNoEventHeightWithIndex:index];
    };
    _viewPager = [[XHHorizonalController alloc] initWithViewControllers:_controllers];
    _viewPager.scrollEnabled = self.isSwipe;
    CGFloat height = self.view.bounds.size.height;
    if (_isOnNav){
         _viewPager.view.frame = self.view.bounds;
    }else{
         _viewPager.view.frame = CGRectMake(0,self.columbarBarHeight,self.view.bounds.size.width, height-self.columbarBarHeight);
    }
    [self addChildViewController:self.viewPager];
    [self.view addSubview:_viewPager.view];
    _viewPager.changeIndex = ^(NSUInteger index) {
        [tempSelf.columbarBar setSelectedItemIndex:index];
        [tempSelf setCollectionViewNoEventHeightWithIndex:index];
    };
    _viewPager.scrollView = ^(CGFloat offsetRatio, NSUInteger focusIndex, NSUInteger animationIndex) {
        [tempSelf.columbarBar scrollViewDidScroll:tempSelf.viewPager.collectionView];
    };
    _viewPager.viewDidAppear = ^(NSInteger index){
        
    }; }
- (void)viewDidLoad{
    [super viewDidLoad];
    self.automaticallyAdjustsScrollViewInsets=NO;
    self.extendedLayoutIncludesOpaqueBars = NO;
    self.view.backgroundColor = [UIColor whiteColor];
    [self dealViewHeight];
    [self initUI];

}
/**
 *  一般不用赋值  此处是为了爆料日期区域不响应滑动
 *
 */
- (void)setCollectionViewNoEventHeightWithIndex:(NSInteger)index{
    return;
}
- (void)viewDidAppear:(BOOL)animated{
    
}
- (void)set_ColumbarBar{
    self.columbarBar.itemTitleColor = self.normalTextColor?:NORMAL_COlOR;
    self.columbarBar.itemTitleSelectedColor = self.selectedTextColor?:SELECT_COlOR;
    self.columbarBar.itemTitleFont = self.normalTextFont?:NORMAL_FONT ;
    self.columbarBar.itemTitleSelectedFont = self.selectedTextFont?:SELECTL_FONT;
     self.columbarBar.leftAndRightSpacing = 20;
    if(self.isFitTextWidth){
       [self.columbarBar setScrollEnabledAndItemFitTextWidthWithSpacing:20];
    }else{
       [self.columbarBar setScrollEnabledAndItemWidth:floor(self.columbarBar.frame.size.width - 40)/_titles.count];
    }

   // if ([ZYAppEnvironment isKXZEnvironMent]) {
       
   // }else{
     //
    //}
    
     self.columbarBar.itemFontChangeFollowContentScroll = YES;
     self.columbarBar.itemSelectedBgScrollFollowContent = YES;
     self.columbarBar.itemSelectedBgColor = [UIColor clearColor];
}

- (NSInteger)currentSelectedIndex{
    return self.columbarBar==nil?0:self.columbarBar.selectedItemIndex;
}
//处理初始高度
-(void)dealViewHeight{
    float viewHeight = ([[UIScreen mainScreen] bounds]).size.height-20;
    if (self.navigationController&&self.navigationController.navigationBarHidden==NO) {
        viewHeight -= self.navigationController.navigationBar.frame.size.height;
    }
    if (self.tabBarController&&self.tabBarController.tabBar.hidden==NO) {
        viewHeight -= self.tabBarController.tabBar.frame.size.height;
    }
    CGRect frame = self.view.frame;
    frame.size.height = viewHeight;
    self.view.frame = frame;
    
}
@end
