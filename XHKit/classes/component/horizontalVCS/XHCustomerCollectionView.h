//
//  ZYCustomerCollectionView.h
//  TImerTest
//
//  Created by guanxuhang1234 on 16/6/15.
//  Copyright © 2016年 guanxuhang1234. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface XHCustomerCollectionView : UICollectionView<UIGestureRecognizerDelegate>
@property(nonatomic,assign)CGFloat noEventHeight;//头部不接受时间的高度
@end
