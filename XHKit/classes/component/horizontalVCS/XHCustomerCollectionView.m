//
//  ZYCustomerCollectionView.m
//  TImerTest
//
//  Created by guanxuhang1234 on 16/6/15.
//  Copyright © 2016年 guanxuhang1234. All rights reserved.
//

#import "XHCustomerCollectionView.h"
@interface XHCustomerCollectionView()
{
    BOOL isJudge;//用判断可控范围吗  不可滑动情况下不用
}
@end
@implementation XHCustomerCollectionView
-(instancetype)init
{
    self = [super init];
    if (self) {

    }
    return self;
}
-(BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
    // 首先判断otherGestureRecognizer是不是系统pop手势
    if ([otherGestureRecognizer.view isKindOfClass:NSClassFromString(@"UILayoutContainerView")]) {
        // 再判断系统手势的state是began还是fail，同时判断scrollView的位置是不是正好在最左边
        if (otherGestureRecognizer.state == UIGestureRecognizerStateBegan && (self.contentOffset.x == 0)) {
            return YES;
        }
    }
    
    return NO;
}

- (void)drawRect:(CGRect)rect{
    [super drawRect:rect];
    isJudge = self.scrollEnabled;
}
- (UIView*)hitTest:(CGPoint)point withEvent:(UIEvent *)event
{
    UIView* result = [super hitTest:point withEvent:event];
    if (isJudge==NO) {
        return result;
    }
 
    if (point.y>=self.noEventHeight)
    {
        self.canCancelContentTouches = NO;
        self.delaysContentTouches = NO;
        self.scrollEnabled = YES;
    }
    else
    {
        self.canCancelContentTouches = YES;
        self.delaysContentTouches = YES;
        self.scrollEnabled = NO;
    }
    return result;
}
@end
