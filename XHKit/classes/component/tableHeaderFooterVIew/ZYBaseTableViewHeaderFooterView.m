//
//  ZYBaseTableViewHeaderFooterView.m
//  ZYLottery
//
//  Created by guanxuhang1234 on 16/6/17.
//  Copyright © 2016年 章鱼彩票. All rights reserved.
//

#import "ZYBaseTableViewHeaderFooterView.h"

@implementation ZYBaseTableViewHeaderFooterView
- (void)setFrame:(CGRect)frame{
    if(_isScrollWithTable==NO){
        [super setFrame:frame];
        return;
    }
    if (_isHeader==YES) {
        if (self.tableView) {
            CGRect sectionRect = [self.tableView rectForSection:self.section];
            CGRect newFrame = CGRectMake(CGRectGetMinX(frame), CGRectGetMinY(sectionRect), CGRectGetWidth(frame), CGRectGetHeight(frame));
            [super setFrame:newFrame];
        }
        else{
            
            [super setFrame:frame];
        }
        return;
    }
    if (self.tableView) {
        CGRect sectionRect = [self.tableView rectForSection:self.section];
        CGRect newFrame = CGRectMake(CGRectGetMinX(frame), CGRectGetMaxY(sectionRect)- CGRectGetHeight(frame), CGRectGetWidth(frame), CGRectGetHeight(frame));
        [super setFrame:newFrame];
    }
    else{
        
        [super setFrame:frame];
    }
    
    
}
@end
