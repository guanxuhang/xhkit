//
//  ZYBaseTableViewHeaderFooterView.h
//  ZYLottery
//
//  Created by guanxuhang1234 on 16/6/17.
//  Copyright © 2016年 章鱼彩票. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ZYBaseTableViewHeaderFooterView : UITableViewHeaderFooterView
@property(nonatomic,weak)UITableView * tableView;
@property(nonatomic,assign)NSInteger section;
@property(nonatomic,assign)BOOL isHeader;
@property(nonatomic,assign)BOOL isScrollWithTable;//是否随着tableview移动
@end
