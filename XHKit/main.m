//
//  main.m
//  XHKit
//
//  Created by guanxuhang1234 on 2017/5/19.
//  Copyright © 2017年 liqing. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
